package BusinessService.Entities;

import java.util.Date;

/**
 * @author Iron Malon
 * @version 1.0
 * @created 17-���-2014 22:07:54
 */
public class Request {

	
	private String address;
	
	private double creditAmmount;
	private boolean errors;
	private String firstName;
	private int id;
	private boolean isApproved;
	private boolean isConfirmed;
	private boolean isError = false;
	private String lastName;
	private String sex;
	

	public Request() {

	}

	public Request(String address, double creditAmmount, String firstName,
			String lastName, String sex) {
		super();
		this.address = address;
		this.creditAmmount = creditAmmount;
		this.firstName = firstName;
		this.lastName = lastName;
		this.sex = sex;
	}

	public Request(String[] fields) {
		
		firstName = fields[0];
		lastName = fields[1];
		sex = fields[2];
		address = fields[3];
		creditAmmount = Double.parseDouble(fields[4]);
		//isError = Boolean.parseBoolean(fields[5]);
	}

	public void finalize() throws Throwable {

	}

	
	/**
	 * 
	 * @param fields
	 */
	public Request createRequest(String[] fields) {
		String firstName = fields[0];
		String lastName = fields[1];
		String sex = fields[2];
		String address = fields[3];
		String creditAmmountString = fields[4];

		return new Request(address, creditAmmount, firstName, lastName, sex);
	}

	/**
	 * 
	 * @param user
	 * @param date
	 */
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	public double getCreditAmmount() {
		return creditAmmount;
	}

	public void setCreditAmmount(double creditAmmount) {
		this.creditAmmount = creditAmmount;
	}

	public boolean isErrors() {
		return errors;
	}

	public void setErrors(boolean errors) {
		this.errors = errors;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}

	public boolean isConfirmed() {
		return isConfirmed;
	}

	public void setConfirmed(boolean isConfirmed) {
		this.isConfirmed = isConfirmed;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	
	public boolean isError() {
		return isError;
	}

	public void setError(boolean isError) {
		this.isError = isError;
	}

}