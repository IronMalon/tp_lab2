package BusinessService.Entities;

/**
 * @author Iron Malon
 * @version 1.0
 * @created 17-���-2014 22:07:54
 */
public class User {

	private int id;
	private String login;
	private String password;
	private int type;

	public User() {

	}

	public User(String login, String password) {
		super();
		this.login = login;
		this.password = password;
	}

	public void finalize() throws Throwable {

	}

	/**
	 * 
	 * @param login
	 * @param password
	 */
	
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}