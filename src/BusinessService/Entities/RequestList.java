package BusinessService.Entities;
import BusinessService.Entities.Request;

import java.util.ArrayList;

/**
 * @author Iron Malon
 * @version 1.0
 * @created 17-���-2014 22:07:54
 */
public class RequestList {

	private ArrayList<Request> requests;
	

	public RequestList(){

	}

	public void finalize() throws Throwable {

	}

	

	/**
	 * 
	 * @param list
	 */
	public void setList(ArrayList<Request> list){

		requests = list;
	}

	public ArrayList<Request> getRequests() {
		return requests;
	}

}