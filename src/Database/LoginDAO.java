package Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;

import Application.Controllers.LoginController;
import BusinessService.Entities.*;

/**
 * @author Iron Malon
 * @version 1.0
 * @created 17-���-2014 22:07:53
 */
public class LoginDAO {

	public LoginDAO() {

	}

	public void finalize() throws Throwable {

	}

	/**
	 * 
	 * @param login
	 * @param password
	 */
	public static User findUser(User user) {

		String login = user.getLogin();
		String password = user.getPassword();
		Statement st;
		ResultSet rs;
		try {
			st = conn.createStatement();
			rs = st.executeQuery("select * from user where login = '" + login
					+ "' and password = '" + password + "'");
			if (rs.next()) {
				Integer uType, id;
				uType = Integer.parseInt(rs.getString("TYPE"));
				id = Integer.parseInt(rs.getString("ID"));
				user.setId(id);
				user.setType(uType);

				rs.close();
				st.close();
				return user;
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static Connection getConn() {
		return conn;
	}

	public static void setConn(Connection conn) {
		LoginDAO.conn = conn;
	}

	private static Connection conn;
}