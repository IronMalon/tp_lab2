package Database;

import java.util.ArrayList;

import BusinessService.Entities.*;

import java.rmi.server.*;
import java.rmi.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

/**
 * @author Iron Malon
 * @version 1.0
 * @created 17-���-2014 22:07:54
 */

public class RequestDAO {

	public RequestDAO() {

	}

	public void finalize() throws Throwable {

	}

	/**
	 * 
	 * @param request
	 */
	

	public static ArrayList<Request> getRequestList() {
		ArrayList<Request> ar = new ArrayList<Request>();
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("select * from request");
			while (rs.next()) {
				String[] s = new String[5];
				s[0] = rs.getString("FIRST_NAME");
				s[1] = rs.getString("LAST_NAME");
				s[2] = rs.getString("SEX");
				s[3] = rs.getString("ADDRESS");
				s[4] = rs.getString("AMMOUNT");

				boolean isError = rs.getBoolean("IS_ERROR");
				if (!isError) {
					int id = Integer.parseInt(rs.getString("ID"));
					Request r = new Request(s);
					r.setId(id);
					ar.add(r);
				}

			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ar;
	}

	/**
	 * 
	 * @param request
	 */
	public static boolean saveRequest(Request request) {
		try {
			String query = "INSERT INTO REQUEST (FIRST_NAME, LAST_NAME, SEX, ADDRESS, AMMOUNT) VALUES (?, ?, ?, ?, ?)";
			java.sql.PreparedStatement st = conn.prepareStatement(query);
			st.setString(1, request.getFirstName());
			st.setString(2, request.getLastName());
			st.setString(3, request.getSex());
			st.setString(4, request.getAddress());
			st.setDouble(5, request.getCreditAmmount());

			st.executeUpdate();
			st.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	

	public static void signRequest(int requestId, int userId, String comments) {
		// TODO Auto-generated method stub
		String query = "UPDATE REQUEST SET CHECKED_BY = ?, COMMENTS = ? WHERE ID = ?";
		try {
			PreparedStatement st = (PreparedStatement) conn
					.prepareStatement(query);
			st.setInt(1, userId);
			st.setString(2, comments);
			st.setInt(3, requestId);
			st.executeUpdate();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void confirmRequest(int id) {
		// TODO Auto-generated method stub
		String query = "UPDATE REQUEST SET IS_CONFIRMED = 1 WHERE ID = ?";
		try {
			PreparedStatement st = (PreparedStatement) conn
					.prepareStatement(query);
			st.setInt(1, id);

			st.executeUpdate();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void setErrors(int id) {
		String query = "UPDATE REQUEST SET IS_ERROR = 1 WHERE ID = ?";
		try {
			PreparedStatement st = (PreparedStatement) conn
					.prepareStatement(query);
			st.setInt(1, id);

			st.executeUpdate();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	public static java.sql.Connection getConn() {
		return conn;
	}

	public static void setConn(java.sql.Connection conn) {
		RequestDAO.conn = conn;
	}
	private static java.sql.Connection conn;
}