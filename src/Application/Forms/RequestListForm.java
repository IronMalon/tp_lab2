package Application.Forms;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableModel;

import Application.Controllers.RequestListController;
import BusinessService.Entities.*;
import Database.RequestDAO;

/**
 * @author Iron Malon
 * @version 1.0
 * @created 17-���-2014 22:07:53
 */
public class RequestListForm extends JFrame {

	
	public RequestListForm(JTable tab) {

		setWindowStyle();
		initComponents(tab);
		add(scroll);
		add(commentsJTextArea);
		add(buttonJPanel);

		buttonJPanel.add(overlookButton);
		buttonJPanel.add(confirmButton);
	}

	public void finalize() throws Throwable {

	}

	
	public void showForm() {

		setVisible(true);
	}


	private void setWindowStyle() {
		Point center = GraphicsEnvironment.getLocalGraphicsEnvironment()
				.getCenterPoint();
		center.x -= DEFAULT_WIDTH / 2;
		center.y -= DEFAULT_HEIGHT / 2;
		setLocation(center);
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		setMinimumSize(new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT));
		setLayout(new GridLayout(3, 1));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}


	private void initButtons() {
		confirmButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

				confirm();
			}
		});

		overlookButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

				sendToOverlook();
			}
		});
	}

	private void initComponents(JTable tab) {
		requestJTable = tab;
		commentsJTextArea = new JTextArea(5, 40);

		confirmButton = new JButton("Confirm");
		overlookButton = new JButton("Send to overlook");

		buttonJPanel = new JPanel();
		scroll = new JScrollPane(requestJTable);
		initButtons();
	}

	public void  confirm() {
		int requestId = getSelectedId();
		String comments = commentsJTextArea.getText();
		System.out.println(comments);
		RequestListController.signRequest(requestId,comments);
		RequestListController.confirmRequest(requestId);
	}

	public void sendToOverlook() {
		int requestId = getSelectedId();
		String comments = commentsJTextArea.getText();
		System.out.println(comments);
		RequestListController.signRequest(requestId,comments);
		RequestListController.sendToOverlook(requestId);
	}
	private int getSelectedId() {
		int row = requestJTable.getSelectedRow();
		TableModel model = requestJTable.getModel();

		String idStr = (String) model.getValueAt(row, 0);
		Integer id = Integer.parseInt(idStr);

		return id;
	}

	private static JTable requestJTable;
	private JTextArea commentsJTextArea;

	private JButton confirmButton;
	private JButton overlookButton;

	private JPanel buttonJPanel;

	private JScrollPane scroll;

	public static final int DEFAULT_HEIGHT = 230;
	public static final int DEFAULT_WIDTH = 450;
}