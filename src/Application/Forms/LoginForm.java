package Application.Forms;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Application.Controllers.LoginController;
import Application.Controllers.RequestListController;
import BusinessService.Entities.User;

import javax.swing.*;

/**
 * @author Iron Malon
 * @version 1.0
 * @created 17-���-2014 22:07:53
 */
public class LoginForm extends JFrame {

	public LoginForm() {

		super("Log In");
		loginTextField = new JTextField(15);
		passwordField = new JPasswordField(15);
		mainPanel = new JPanel(new GridLayout(7, 4));
		submitButton = new JButton("Ok");
		setWindowStyle();

		mainPanel.add(new JLabel("Login:"));
		mainPanel.add(loginTextField);

		addFakePanel(7);
		mainPanel.add(new JLabel("Password:"));
		mainPanel.add(passwordField);

		addFakePanel(5);
		mainPanel.add(submitButton);

		add(mainPanel, BorderLayout.WEST);

		submitButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				submitLogin();

			}
		});
	}

	public void finalize() throws Throwable {

	}

	public void showForm() {

		setVisible(true);
	}

	public void submitLogin() {
		String login = loginTextField.getText();
		String password = passwordField.getText();
		
		showResultMessage(LoginController.login(login, password));
	}

	private void setWindowStyle() {
		Point center = GraphicsEnvironment.getLocalGraphicsEnvironment()
				.getCenterPoint();
		center.x -= DEFAULT_WIDTH / 2;
		center.y -= DEFAULT_HEIGHT / 2;
		setLocation(center);
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		setMinimumSize(new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT));
		setLayout(new BorderLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void addFakePanel(int panelNumber) {
		for (int i = 0; i < panelNumber; ++i)
			mainPanel.add(new JPanel());
	}

	private  void showResultMessage(int userType) {
		if (userType != 0){
			JOptionPane.showMessageDialog(null, "You have logged in",
					"Login", JOptionPane.INFORMATION_MESSAGE);
			LoginController.showNextForm(userType);
			setVisible(false);
			
		}
		else{
			JOptionPane.showMessageDialog(null, "No such user/password",
					"Login error", JOptionPane.ERROR_MESSAGE);
			
		}
	}

	private JPanel mainPanel;
	private JTextField loginTextField;
	private JPasswordField passwordField;
	private JButton submitButton;

	public static final int DEFAULT_HEIGHT = 230;
	public static final int DEFAULT_WIDTH = 450;
}