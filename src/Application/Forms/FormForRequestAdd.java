package Application.Forms;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.StreamCorruptedException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Application.Controllers.RequestListController;

/**
 * @author Iron Malon
 * @version 1.0
 * @created 17-���-2014 22:07:53
 */
public class FormForRequestAdd extends JFrame {

	//public RequestListController m_RequestListController;

	public FormForRequestAdd() {

		super("Add new request");
		setWindowStyle();
		initComponents();
		addComponentsToPanel();
		add(mainPanel, BorderLayout.CENTER);

	}

	public void finalize() throws Throwable {

	}

	

	public void showForm() {

		setVisible(true);
	}

	public void submitForm() {

		String[] fields = new String[5];
		fields[0] = firstNameTextArea.getText();
		fields[1] = lastNameTextArea.getText();
		fields[2] = sexTextArea.getText();
		fields[3] = addressTextArea.getText();
		fields[4] = creditAmmountTextArea.getText();

		RequestListController.createRequest(fields);

	}

	private void setWindowStyle() {
		Point center = GraphicsEnvironment.getLocalGraphicsEnvironment()
				.getCenterPoint();
		center.x -= DEFAULT_WIDTH / 2;
		center.y -= DEFAULT_HEIGHT / 2;
		setLocation(center);
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		setMinimumSize(new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT));
		setLayout(new BorderLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void addFakePanel(int panelNumber) {
		for (int i = 0; i < panelNumber; ++i)
			mainPanel.add(new JPanel());
	}

	private void initComponents() {
		mainPanel = new JPanel(new GridLayout(6, 2));

		firstNameTextArea = new JTextField(15);
		lastNameTextArea = new JTextField(15);
		sexTextArea = new JTextField(15);
		addressTextArea = new JTextField(15);
		creditAmmountTextArea = new JTextField(15);

		submitButton = new JButton("Ok");
		initButton();
	}

	private void addComponentsToPanel() {
		mainPanel.add(new JLabel("First name:"));
		mainPanel.add(firstNameTextArea);
		// addFakePanel(2);

		mainPanel.add(new JLabel("Last name:"));
		mainPanel.add(lastNameTextArea);
		// addFakePanel(2);

		mainPanel.add(new JLabel("Sex:"));
		mainPanel.add(sexTextArea);
		// addFakePanel(2);

		mainPanel.add(new JLabel("Address:"));
		mainPanel.add(addressTextArea);
		// addFakePanel(2);

		mainPanel.add(new JLabel("Credit ammount:"));
		mainPanel.add(creditAmmountTextArea);

		addFakePanel(1);

		mainPanel.add(submitButton);
	}

	private void initButton() {
		submitButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				submitForm();
				JOptionPane.showMessageDialog(null,
						"The request has been added", "Success",
						JOptionPane.INFORMATION_MESSAGE);

			}
		});
	}

	private JPanel mainPanel;
	private JButton submitButton;

	private JTextField firstNameTextArea;
	private JTextField lastNameTextArea;
	private JTextField sexTextArea;
	private JTextField addressTextArea;
	private JTextField creditAmmountTextArea;

	public static final int DEFAULT_HEIGHT = 450;
	public static final int DEFAULT_WIDTH = 330;
}