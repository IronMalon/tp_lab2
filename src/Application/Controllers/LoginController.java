package Application.Controllers;

import java.sql.Connection;

import Database.LoginDAO;
import Application.Forms.LoginForm;
import BusinessService.Entities.User;

/**
 * @author Iron Malon
 * @version 1.0
 * @created 17-���-2014 22:07:53
 */
public class LoginController {

	private LoginForm m_LoginForm;
	

	public LoginController() {

		m_LoginForm = new LoginForm();
		m_LoginForm.showForm();
	}

	public void finalize() throws Throwable {

	}

	/**
	 * 
	 * @param login
	 * @param password
	 */
	public static int login(String login, String password) {

		User currentUser = new User(login, password);

		RequestListController.setUser(currentUser);

		if (LoginDAO.findUser(currentUser) != null)
			return currentUser.getType();
		else
			return 0;
	}

	public static void showNextForm(int userType) {
		switch (userType) {
		case 1:
			RequestListController.showRequestAddForm();
			break;

		case 2:
			RequestListController.showRequestListForm();
			break;
		default:
			break;
		}
	}

}