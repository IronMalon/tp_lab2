package Application.Controllers;

import java.sql.Connection;

import javax.swing.JTable;

import Application.Forms.FormForRequestAdd;
import Database.RequestDAO;
import BusinessService.Entities.Request;
import BusinessService.Entities.User;
import Application.Forms.RequestListForm;
import BusinessService.Entities.RequestList;

/**
 * @author Iron Malon
 * @version 1.0
 * @created 17-���-2014 22:07:53
 */
public class RequestListController {

	private static FormForRequestAdd m_FormForRequestAdd;

	private static RequestListForm m_RequestListForm;
	private static User user;
	private RequestList m_RequestList;

	public RequestListController() {

		m_FormForRequestAdd = new FormForRequestAdd();
		m_RequestListForm = new RequestListForm(createTable());

	}

	public void finalize() throws Throwable {

	}

	public static void confirmRequest(int id) {
		RequestDAO.confirmRequest(id);

	}

	/**
	 * 
	 * @param requestID
	 */
	public static boolean createRequest(String[] fields) {
		Request m_Request = new Request(fields);
		RequestDAO.saveRequest(m_Request);
		return false;
	}

	

	/**
	 * 
	 * @param id
	 */
	public static void sendToOverlook(int id) {

		RequestDAO.setErrors(id);
	}

	public static void showRequestAddForm() {
		m_FormForRequestAdd.showForm();
	}

	public static void showRequestListForm() {
		m_RequestListForm.showForm();
	}

	/**
	 * 
	 * @param comments
	 * @param user
	 */
	public static void signRequest(int id, String comments) {

		RequestDAO.signRequest(id, user.getId(), comments);

	}

	

	private JTable createTable() {
		m_RequestList = new RequestList();
		m_RequestList.setList(RequestDAO.getRequestList());

		String[] columnNames = { "ID", "First name", "Last Name", "Sex",
				"Address", "Credit Ammount" };
		int requestNumber = m_RequestList.getRequests().size();

		Object[][] tab = new String[requestNumber][6];

		for (int i = 0; i < requestNumber; ++i) {

			Request r = m_RequestList.getRequests().get(i);
			tab[i][0] = (new Integer(r.getId())).toString();
			tab[i][1] = r.getFirstName();
			tab[i][2] = r.getLastName();
			tab[i][3] = r.getSex();
			tab[i][4] = r.getAddress();

			tab[i][5] = (new Double(r.getCreditAmmount())).toString();

		}

		return new JTable(tab, columnNames);
	}

	

	public static User getUser() {
		return user;
	}

	public static void setUser(User user) {
		RequestListController.user = user;
	}

}