import javax.swing.JFrame;

import Application.Controllers.LoginController;
import Application.Controllers.RequestListController;
import Application.Forms.LoginForm;
import Application.Forms.RequestListForm;
import Database.LoginDAO;
import Database.RequestDAO;

import java.io.*;
import java.sql.*;


public class FrameTest {

	public static void main(String[] arg){
		//
		
		Connection conn = databaseConnection();
		LoginDAO.setConn(conn);
		RequestDAO.setConn(conn);
		RequestListController rContr = new RequestListController();
		LoginController logContr = new LoginController();
		//RequestListForm rForm = new RequestListForm();
		//rForm.showForm();
		
		
	}
	
	public static Connection databaseConnection() {
		Connection conn = null;
		try {
			String userName = "root";
			String password = "gibson";
			String url = "jdbc:mysql://localhost/tp";
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			System.out.println("Database connection established");
		} catch (Exception e) {
			System.err.println("Cannot connect to database server");
			e.printStackTrace();
		} finally {
			
		}
		return conn;
	}
}
